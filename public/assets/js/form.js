


let contactForm = document.querySelector("#contact-form")

contactForm.addEventListener("submit", (e) => {

	e.preventDefault()

	let name, email, subject, message

	name = document.querySelector("#name").value
	email = document.querySelector("#email").value
	subject = document.querySelector("#subject").value
	message = document.querySelector("#message").value

	/*console.log(name)
	console.log(email)
	console.log(subject)
	console.log(message)*/

	const payload = {

		method: 'POST',
		headers: {

			'Content-Type': 'application/json',
			
		},
		body: JSON.stringify({

			name: name,
			email: email,
			subject: subject,
			message: message

		})

	}


	
	fetch('https://dry-cliffs-93822.herokuapp.com/contact-me/email', payload)
	.then(res => res.json())
	.then(data => {

		if(data===true){
			
			Swal.fire({
			  
			  icon:'success',
			  title:'Message Sent!',
			  text:'Please check your email for a reply soon.',
			  confirmButtonText: 'Thanks! I will'
			})

		} else {

			Swal.fire({
			  
			  icon: 'error',
			  title: 'Oops...',
			  text: 'Something went wrong!',
			  confirmButtonText: 'Please try again'
			
			})
		}
	})	


})



